Hola!

En principio, el endpoint de Music Match devuelve errores de CORS (allow-origin), y desde el front la solución más viable que encontré fue utilizar un reverse proxy para poder hacer el ejercicio. (https://cors-anywhere.herokuapp.com/) <- Hay que activarlo yendo a este link.

/.env
En este archivo están las constantes de conexión. (PROXY + URL ENDPOINT y API_KEY)

Para obtener el proyecto simplemente desde una terminal con nodeJs e git instalados globalmente, ejecutar git clone https://gitlab.com/tokkyoestudio/aranda-soft-test-1.git.
Luego para correr la aplicación en desarrollo http://localhost:3000 hay que entrar en la carpeta clonada y ejecutar "npm i" para reinstalar los node_modules y "npm start".
Se abrirá una pestaña desde el navegador default de su computadora.

Aclaraciones, no tuve la necesidad de usar una librería store, como Redux, Mobx, Zustand, etc. En este caso opté simplemente por props y context.

Para el modal usé React.Portals.

Para las llamadas fetch refactoricé a un archivo en la carpeta api.

Traté de evitar todas las librerías 3rd parties, sólo use node-sass.

Respecto al CSS usé simplemente SCSS, y utilicé los atributos nativos de CSS Grid, evitando usar librerías pesadas como Bootstrap, etc.. En lo personal me atraen mucho soluciones CSS-JS como styles-components, modules, que facilitan la integración de variables y el testing al tomar al css como objeto.

Los errores de response de endpoint tienen el siguiente mensaje [status_code] Por favor vuelva a intentarlo más tarde.

Hubiera querido poder refactorizar mucho más, hay componentes a los que le falta bastante depuración, pero me pidieron si podía responderlo antes del fin de semana y en 2 días y con mi trabajo a la vez, es lo que pude. Igualmente creo que se puede apreciar algo. Es posible encontrar algún bug también, por la misma razón de escasez temporal.

En los casos en los cuales los artistas no tenían twitter o detalles, omití el botón de ver más y el bird. Los favs pueden generarse directamente del listado.

Enormes gracias por la oportunidad!

Julio
