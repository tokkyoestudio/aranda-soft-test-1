import React, { useState, useEffect } from "react";

import "./App.scss";
import ArtistList from "./components/ArtistsList";
import Buscador from "./components/Buscador";
import Loading from "./components/shared/Loading";
import ErrorCode from "./components/shared/ErrorCode";
import { ArtistsContext } from "./context/Artists";
import customFetch from "./api/customFetch";

export default function App() {
	const [artists, setArtists] = useState([]);
	const [errorCode, setErrorCode] = useState(false);
	const [loading, setLoading] = useState(false);
	const [favs, setFavs] = useState(false);
	const [inicio, setInicio] = useState(false);
	const [selected, setSelected] = useState([]);

	useEffect(() => {
		async function getArtists() {
			setErrorCode(false);
			setLoading(() => true);
			const resp = await customFetch(
				"chart.artists.get?page=1&page_size=12"
			);
			if (resp.status && resp.status !== 200) setErrorCode(() => resp.status);
			else {
				const data = await resp.json();			
				if (data?.message?.body?.artist_list)
					setArtists(data?.message?.body?.artist_list);
				else if (data?.message?.header?.status_code !== 200)
					setErrorCode(() => data?.message?.header?.status_code);
			}
			setLoading(() => false);
			setInicio(() => false);
			setFavs(() => false);
		}
		getArtists();
	}, [inicio]);

	const handleFavs = () => {
		let existing = localStorage.getItem("idFavs");
		existing = existing ? existing.split(",") : [];
		const array = existing.map((exist) => ({
			artist: {
				artist_id: parseInt(exist.split("|")[0]),
				artist_name: exist.split("|")[1],
				artist_country: exist.split("|")[2],
				artist_twitter_url: exist.split("|")[3],
				begin_date: exist.split("|")[4],
			},
		}));
		setFavs(true);
		setArtists(array);
	};

	const handleSelectedFav = (selected) => {
		let existing = localStorage.getItem("idFavs");
		existing = existing ? existing.split(",") : [];
		const arrayFavSelectedId = existing.map((exist) =>
			parseInt(exist.split("|")[0])
		);
		setSelected(() => arrayFavSelectedId);
		if(favs) handleFavs();
	};

	if (loading) return <Loading />;
	if (errorCode) return <ErrorCode code={errorCode} />;

	return (
		<div className="container">
			<ArtistsContext.Provider value={[artists, setArtists]}>
				<Buscador />
				{!favs ? (
					<button className="buttonFav" onClick={handleFavs}>
						Favoritos
					</button>
				) : (
					<button className="buttonFav" onClick={() => setInicio(true)}>
						Inicio
					</button>
				)}
				<ArtistList
					title={favs ? "Favoritos" : "Top 12 by MusicMatch"}
					favSelected={handleSelectedFav}
					favs={favs}
				/>
			</ArtistsContext.Provider>
		</div>
	);
}
