const { REACT_APP_URL_API, REACT_APP_API_KEY } = process.env;

const customFetch = async (params) => {
	const get = await fetch(
		`${REACT_APP_URL_API}${params}&apikey=${REACT_APP_API_KEY}`,
		{ method: "GET" }
	);
	return get;
};

export default customFetch;
