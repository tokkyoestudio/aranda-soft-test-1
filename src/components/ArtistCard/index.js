import React, { useState } from "react";

import "./styles.scss";
import Icon from "../shared/Icon";

const ArtistCard = ({
	artist: {
		artist_name,
		artist_country,
		artist_twitter_url,
		begin_date,
		artist_id,
	},
	openModal,
	fav,
}) => {
	const handleFav = (existe) => {
		fav(
			artist_id,
			artist_name,
			artist_country,
			artist_twitter_url,
			begin_date,
			existe ? "removeFav" : "addFav"
		);
	};

	const actionFav = () => {
		let existing = localStorage.getItem("idFavs");
		existing = existing ? existing.split(",") : [];
		const existe = existing.some(
			(exists) => parseInt(exists.split("|")[0]) === artist_id
		);

		return (
			<Icon
				classes={`${existe ? "fas" : "far"} fa-star`}
				click={() => handleFav(existe)}
			/>
		);
	};

	return (
		<div className="card">
			<h2>Artista: {artist_name}</h2>
			<h4>País: {artist_country ? artist_country : "No existen datos"}</h4>
			<h5>
				Fecha de lanzamiento:
				{begin_date === "0000-00-00"
					? " No especificada"
					: ` ${begin_date}`}
			</h5>
			{actionFav()}
			{artist_twitter_url && (
				<a href={artist_twitter_url} rel="noreferrer" target="_blank">
					<Icon classes="fab fa-twitter" />
				</a>
			)}
			{begin_date !== "0000-00-00" && (
				<Icon
					classes="fas fa-plus-circle"
					click={() => openModal(artist_id, artist_name)}
				/>
			)}
		</div>
	);
};

export default ArtistCard;
