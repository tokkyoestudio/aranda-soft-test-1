import React, { useState, useContext } from "react";

import "./styles.scss";
import ArtistCard from "../ArtistCard";
import ModalWrapper from "../ModalWrapper";
import { ArtistsContext } from "../../context/Artists";

const ArtistList = ({ title, favSelected, favs }) => {
	const [showModal, setShowModal] = useState(false);
	const [id, setId] = useState(0);
	const [name, setName] = useState("");
	const [artists] = useContext(ArtistsContext);
	if (localStorage.getItem("idFavs") === null) {
		localStorage.setItem("idFavs", []);
	}

	const handleModal = (id, name) => {
		setShowModal((prevState) => true);
		setId((prevState) => id);
		setName((prevState) => name);
	};

	const handleHideModal = () => setShowModal((prevState) => false);

	const handleFav = (
		artist_id,
		artist_name,
		artist_country,
		artist_twitter_url,
		begin_date,
		actionFav
	) => {
		let existing = localStorage.getItem("idFavs");
		existing = existing ? existing.split(",") : [];
		const item = artist_id +
		"|" +
		artist_name +
		"|" +
		artist_country +
		"|" +
		artist_twitter_url +
		"|" +
		begin_date;
		if (actionFav === "addFav") existing.push(item);
		else {
			const set = new Set(existing);
			set.delete(item);
			existing = Array.from(set);			
		}
		localStorage.setItem("idFavs", existing.toString());
		favSelected();
	};

	const artistsMap = () => {
		return artists.map((artist) => (
			<ArtistCard
				key={artist.artist.artist_id}
				{...artist}
				openModal={handleModal}
				fav={handleFav}
			/>
		));
	};

	return (
		<>
			{artists.length === 0 && <img src="/img/polar-bear.png" alt="Sorry" />}
			<h1>{artists.length === 0 && favs ? "No hay favoritos seleccionados." : title}</h1>
			{artists.length > 0 && <div className="list">{artistsMap()}</div>}
			{showModal && (
				<ModalWrapper
					show={showModal}
					id={id}
					name={name}
					hideModal={handleHideModal}
				/>
			)}
		</>
	);
};

export default ArtistList;
