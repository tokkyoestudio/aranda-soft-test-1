import React, { useState, useEffect, useContext } from "react";

import "./styles.scss";
import Icon from "../shared/Icon";
import customFetch from "../../api/customFetch";
import { ArtistsContext } from "../../context/Artists";

export default function Buscador () {
	const [term, setTerm] = useState("");
	const [_, setArtists] = useContext(ArtistsContext);
	const [searching, setSearching] = useState(false);
	const [reStart, setReStart] = useState(false);

	useEffect(() => {
		const search = async () => {
			const resp = await customFetch(
				reStart
					? "chart.artists.get?page=1&page_size=12"
					: `artist.search?q_artist=${term}`
			);
			const data = await resp.json();
			setArtists(() => data?.message?.body?.artist_list);
			setSearching(false);
		};
		if (searching || reStart) search();
	}, [term, searching, reStart, setArtists]);

	const handleReStart = () => {
		setTerm((prevState) => "");
		setReStart((prevState) => true);
	};

	return (
		<div className="input-group">
			<span className="input-group-text">
				<Icon classes="fas fa-search" />
			</span>
			<input
				type="text"
				value={term}
				placeholder="Buscar"
				onChange={(e) => setTerm(e.target.value)}
			/>
			{term !== "" && (
				<>
					<span className="input-group-text erase">
						<Icon classes="fas fa-times" click={handleReStart} />
					</span>
					<span
						className="input-group-text last"
						onClick={() => setSearching(true)}
					>
						Buscar
					</span>
				</>
			)}
		</div>
	);
};
