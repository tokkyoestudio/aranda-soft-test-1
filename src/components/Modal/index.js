import React from "react";
import ReactDOM from "react-dom";

import "./styles.scss";
import Icon from '../shared/Icon';

const Modal = (props) => {
	const renderAlbums = () => {
		return props.albums.map((album) => {
			return (
				<ul key={album.id} onClick={() => props.getTracks(album.id)}>
					{album.name}
               {album.tracks && album.tracks.map(track => <li key={track.track.track_id}>{track.track.track_name}</li>)}
				</ul>
			);
		});
	};

	return ReactDOM.createPortal(
		<div className={`modal-bg ${props.show && "active"}`}>
			<div className="card box">
				<div className="header">
					{props.name}
					<Icon classes="fas fa-times" click={() => props.hideModal()} />
				</div>
				<div className="content">
					<h4>Álbums</h4>
					{renderAlbums()}
				</div>
			</div>
		</div>,
		document.querySelector("#modal")
	);
};

export default Modal;
