import React, { useEffect, useState } from "react";

import Modal from "../Modal";
import customFetch from '../../api/customFetch';

const ModalWrapper = (props) => {
	const [albums, setAlbums] = useState([]);
	const [albumTracks, setAlbumTracks] = useState(0);

	useEffect(() => {
		async function getArtist() {
			const resp = await customFetch(`artist.albums.get?artist_id=${props.id}&s_release_date=desc&g_album_name=1&page_size=100`);
			const data = await resp.json();
			const ids = new Set();
			if (data?.message?.body?.album_list.length > 0) {
				data?.message?.body?.album_list.map((album) =>
					ids.add({
						id: album.album.album_id,
						name: album.album.album_name,
					})
				);
			}
			setAlbums(() => Array.from(ids));
		}
		getArtist();
	}, [props.id]);

	useEffect(() => {
		async function getTracks() {
			const resp = await customFetch(`album.tracks.get?album_id=${albumTracks}&page=1&page_size=2`);
			const data = await resp.json();

			if (data?.message?.body?.track_list) {
				const addTracks = albums.map((album) => ({
					...album,
					tracks:
						album.id === albumTracks
							? data?.message?.body?.track_list
							: undefined,
				}));
				setAlbums((prevState) => addTracks);
			}
		}

		if (albumTracks !== 0) getTracks();
	}, [albumTracks, albums]);

	return (
		<Modal
			albums={albums}
			name={props.name}
			show={props.show}
			hideModal={props.hideModal}
			getTracks={(id) => setAlbumTracks(id)}
		/>
	);
};

export default ModalWrapper;