import React from "react";

import "./styles.scss";

const ErrorCode = ({ code }) => (
	<div className="errorcode-wrapper">
		<div className="errorcode">{`[${code}] Por favor vuelva a intentarlo más tarde.`}</div>
	</div>
);

export default ErrorCode;
