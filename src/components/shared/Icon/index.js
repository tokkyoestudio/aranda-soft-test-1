import React from "react";

import './styles.scss';

const Icon = ({ classes, click }) => <i className={classes} onClick={click} />;

export default Icon;
