import React from "react";

import "./styles.scss";

const Loading = () => (
	<div className="loading-wrapper">
		<div className="loading"></div>
	</div>
);

export default Loading;
